class SceneCredits : IScene
{
	SceneCredits()
	{
		@controller = loadController("Menu/Menu.axm");
		setGameView("Black");
		
		@menu = controller.GetWidget("Menu");
		menu.ConnectClicked(OnClickCallback(this.OnMenu));
		menu.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnMenu));
		menu.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffMenu));
		@menuLabel = controller.GetLabel("Menu");
		
		@exit = controller.GetWidget("Exit");
		exit.ConnectClicked(OnClickCallback(this.OnExit));
		exit.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnExit));
		exit.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffExit));
		@exitLabel = controller.GetLabel("Exit");
		
		setFontSize(48);
		setFontName("Times Sans Serif");
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	void OnMenu()
	{
		activateScene("SceneMenu");
	}
	
	void OnMoveOnMenu()
	{
		menuLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffMenu()
	{
		menuLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnMoveOnExit()
	{
		exitLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffExit()
	{
		exitLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnExit()
	{
		quit();
	}
		
	Widget@ menu, exit;
	Label@ menuLabel, exitLabel;
	GuiController@ controller;
}