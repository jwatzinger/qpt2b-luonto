class SceneCredits : IScene
{
	int counter = 0;
	
	SceneCredits()
	{
		@controller = loadController("Menu/Menu.axm");
		setGameView("Black");
				
		log(controller.GetWidget("Area").GetHeight());
		//controller.GetWidget("Area").SetHeight(500);
		
		@menu = controller.GetWidget("Menu");
		menu.ConnectClicked(OnClickCallback(this.OnMenu));
		menu.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnMenu));
		menu.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffMenu));
		@menuLabel = controller.GetLabel("Menu");
		
		@exit = controller.GetWidget("Exit");
		exit.ConnectClicked(OnClickCallback(this.OnExit));
		exit.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnExit));
		exit.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffExit));
		@exitLabel = controller.GetLabel("Exit");
		
		setFontSize(48);
		setFontName("Times Sans Serif");
		setCursorVisible(true);
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
		//counter++;
		//if (counter==50)
		//{
			float height = controller.GetWidget("Area").GetHeight() * 1.0f;
			//controller.GetWidget("Area").SetY((controller.GetWidget("Area").GetY() * 1.0f - 0.001f));
			
			float factor = 0.01f;
		//}
	}
	
	void OnMenu()
	{
		activateScene("SceneMenu");
	}
	
	void OnMoveOnMenu()
	{
		menuLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffMenu()
	{
		menuLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnMoveOnExit()
	{
		exitLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffExit()
	{
		exitLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnExit()
	{
		quit();
	}
		
	Widget@ menu, exit, credits, area;
	Label@ menuLabel, exitLabel;
	GuiController@ controller;
}