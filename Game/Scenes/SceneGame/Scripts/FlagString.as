class FlagString : Flags
{
	FlagString(const Entity& in e)
	{
		const uint NUM_FLAGS = 1;
		super(NUM_FLAGS, 1.0f, 1.0f, Vector3(0, 0, 0), Vector3(0, 0, 0));
		
		rope = createEntity();
		AttachActor(rope, "Rope");
		AttachSoftBody(rope, 2.5f, 0.01f, NUM_FLAGS*5+2, 2, NUM_FLAGS+0.4f, 0.1f, Vector3(1.0f, 0.0f, 0.0f));
		
		ropeIsFixed = true;
	}
	
	FlagString(uint numFlags, bool fixRope, const Vector3& in vPosition, const Vector3& in vTarget)
	{
		const Vector3 vDir = vTarget - vPosition;
			
		super(numFlags, 1.0f, 1.0f, vPosition, vDir);
		
		rope = createEntity();
		AttachActor(rope, "Rope");
		AttachPosition(rope, vPosition);
		
		AttachSoftBody(rope, 2.5f, 0.01f, 7 + (numFlags-1)*4, 2, vDir.length(), 0.1f, vDir.normal());
		
		ropeIsFixed = fixRope;
	}
	
	~FlagString()
	{
		removeEntity(rope);
	}
	
	void OnInit(void)
	{
		SoftBodyComp@ ropeSoftBody = GetSoftBody(rope);
		ropeSoftBody.body.SetMass(0, 0, 0.0f);
		
		if(ropeIsFixed)
			ropeSoftBody.body.SetMass(ropeSoftBody.body.GetSize().x-1, 0, 0.0f);
		
		for(uint i = 0; i < flags.length(); i++)
		{
			RemovePosition(flags[i]);
			SoftBodyComp@ softBody = GetSoftBody(flags[i]);
			
			const uint flagSize = softBody.body.GetSize().x;
			
			for(uint j = 0; j < flagSize; j++)
			{
				softBody.body.SetMass(j, 2.0f);
				softBody.body.SetMass(j+flagSize, 2.0f);
			}

			const uint id = i*4;
			
			ropeSoftBody.body.AddConstraint(id+5, softBody.body, 0);
			ropeSoftBody.body.AddConstraint(id+5, softBody.body, 1);
			
			ropeSoftBody.body.AddConstraint(id+4, softBody.body, flagSize/5);
			ropeSoftBody.body.AddConstraint(id+4, softBody.body, flagSize/5+1);
			
			ropeSoftBody.body.AddConstraint(id+3, softBody.body, uint(flagSize*0.45f));
			
			ropeSoftBody.body.AddConstraint(id+2, softBody.body, uint(flagSize*0.625f));
			ropeSoftBody.body.AddConstraint(id+2, softBody.body, uint(flagSize*0.625f)+1);
			
			ropeSoftBody.body.AddConstraint(id+1, softBody.body, flagSize-2);
			ropeSoftBody.body.AddConstraint(id+1, softBody.body, flagSize-1);
			
			const uint id2 = id+ropeSoftBody.body.GetSize().x;
			
			ropeSoftBody.body.AddConstraint(id2+5, softBody.body, flagSize);
			ropeSoftBody.body.AddConstraint(id2+5, softBody.body, flagSize+1);
			
			ropeSoftBody.body.AddConstraint(id2+4, softBody.body, flagSize+flagSize/5);
			ropeSoftBody.body.AddConstraint(id2+4, softBody.body, flagSize+flagSize/5+1);
			
			ropeSoftBody.body.AddConstraint(id2+3, softBody.body, flagSize+uint(flagSize*0.45f));
			
			ropeSoftBody.body.AddConstraint(id2+2, softBody.body, flagSize+uint(flagSize*0.625f));
			ropeSoftBody.body.AddConstraint(id2+2, softBody.body, flagSize+uint(flagSize*0.625f)+1);
			
			ropeSoftBody.body.AddConstraint(id2+1, softBody.body, flagSize+flagSize-2);
			ropeSoftBody.body.AddConstraint(id2+1, softBody.body, flagSize+flagSize-1);
		}
	}
	
	Entity rope;
	
	bool ropeIsFixed;
}