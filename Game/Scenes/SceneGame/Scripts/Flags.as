class Flags : IGameScript
{
	Flags(uint numFlags, float flagWidth, float flagHeight, const Vector3& in vOffset, const Vector3& in vDir)
	{
		string[] materials;
		materials.insertLast("BlueFlag");
		materials.insertLast("WhiteFlag");
		materials.insertLast("RedFlag");
		materials.insertLast("GreenFlag");
		materials.insertLast("YellowFlag");
		
		for(uint i = 0; i < numFlags; i++)
		{
			Entity flag = createEntity();
			AttachPosition(flag, vOffset + Vector3(i*2.0f - numFlags*0.5f, 0.0f, 0.0f));
			AttachActor(flag, materials[i%5]);
			AttachSoftBody(flag, 0.5f, 0.25f, 20, 16, flagWidth, flagHeight, vDir);
			AttachTransform(flag);
			
			flags.insertLast(flag);
		}
		
		needsInit = true;
	}
	
	~Flags()
	{
		for(uint i = 0; i < flags.length(); i++)
		{
			removeEntity(flags[i]);
		}
	}
	
	void OnUpdate(double dt)
	{
		if(needsInit)
		{
			OnInit();
			
			needsInit = false;
		}
	}
	
	void OnInit(void)
	{
	}
	
	void OnProcessInput(const MappedInput@ input)
	{
	}
	
	Entity[] flags;
	
	bool needsInit;
}