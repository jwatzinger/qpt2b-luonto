class SceneGame : IScene
{
	SceneGame()
	{
		@controller = loadController("Menu/Menu.axm");
		
		activateCamera("Player");
		setGameView("final scene hdr");
		setCursorVisible(false);
		
		@flag = FlagString(5, true, Vector3(299.75, 8.25, 400.5), Vector3(298.75, 8.45, 407));
		@treeFlag = FlagString(15, true, Vector3(267.457, 12.0, 873.983), Vector3(291.079, 9, 880.013));
		
		setWind(Vector3(-1.5f, 0.0f, 3.0f));
		
		setFontSize(48);
		setFontName("Times Sans Serif");
	}
	
	void OnUninit()
	{
	}
	
	void OnUpdate(double dt)
	{
		flag.OnUpdate(dt);
		treeFlag.OnUpdate(dt);
		
		if(TEXT_BOX_TRIGGER.HasText())
		{
			TextArea@ box = controller.GetTextArea("TextBox");
			box.SetText(TEXT_BOX_TRIGGER.PopText());
						
			Widget@ side = controller.GetWidget("Side");
			side.SetVisible(true);
		}
		else if(TEXT_BOX_TRIGGER.CheckCloseBox())
		{
			Widget@ side = controller.GetWidget("Side");
			side.SetVisible(false);
		}
	}
	
	FlagString@ flag, treeFlag;
	GuiController@ controller;
}