class SceneIsland : IScene
{
	SceneIsland()
	{
		activateCamera("Free");
		setGameView("final scene hdr");
		
		@controller = loadController("Menu/HUD.axm");
		controller.GetWidget("Overhead").ConnectClicked(OnClickCallback(this.OnOverheadCam));
		controller.GetWidget("Free").ConnectClicked(OnClickCallback(this.OnFreeCam));
		controller.GetWidget("Tank").ConnectClicked(OnClickCallback(this.OnTankCam));
		controller.GetWidget("Menu").ConnectClicked(OnClickCallback(this.OnMenu));
		
		setFontSize(16);
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	void OnOverheadCam()
	{
		controller.GetWidget("Overhead").SetVisible(false);
		controller.GetWidget("Free").SetVisible(true);
		
		activateCamera("Overhead");
	}

	void OnFreeCam()
	{
		controller.GetWidget("Free").SetVisible(false);
		controller.GetWidget("Tank").SetVisible(true);
		
		activateCamera("Free");
	}

	void OnTankCam()
	{
		controller.GetWidget("Tank").SetVisible(false);
		controller.GetWidget("Overhead").SetVisible(true);
		
		activateCamera("Tank");
	}
	
	void OnMenu()
	{
		activateScene("SceneMenu");
	}
	
	int i;
	uint ui;
	bool b;
	GuiController@ controller;
}