class SceneJohanna : IScene
{
	SceneJohanna()
	{
		activateCamera("Player");
		setGameView("final scene hdr");
		
		@controller = loadController("Menu/HUD.axm");
		controller.GetWidget("Player").ConnectClicked(OnClickCallback(this.OnPlayerCam));
		controller.GetWidget("Free").ConnectClicked(OnClickCallback(this.OnFreeCam));
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	void OnFreeCam()
	{
		controller.GetWidget("Player").SetVisible(true);
		controller.GetWidget("Free").SetVisible(false);
		
		activateCamera("Free");
	}

	void OnPlayerCam()
	{
		controller.GetWidget("Free").SetVisible(true);
		controller.GetWidget("Player").SetVisible(false);
		
		activateCamera("Player");
	}
	
	GuiController@ controller;
}