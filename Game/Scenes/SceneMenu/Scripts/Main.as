class SceneMenu : IScene
{
	SceneMenu()
	{
		@controller = loadController("Menu/Menu.axm");
		setGameView("Black");
		
		@play = controller.GetWidget("Play");
		play.ConnectClicked(OnClickCallback(this.OnPlay));
		play.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnPlay));
		play.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffPlay));
		@playLabel = controller.GetLabel("Play");
		
		@exit = controller.GetWidget("Exit");
		exit.ConnectClicked(OnClickCallback(this.OnExit));
		exit.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnExit));
		exit.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffExit));
		@exitLabel = controller.GetLabel("Exit");
		
		@settings = controller.GetWidget("Settings");
		settings.ConnectClicked(OnClickCallback(this.OnSettings));
		settings.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnSettings));
		settings.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffSettings));
		@settingsLabel = controller.GetLabel("Settings");
		
		@credits = controller.GetWidget("Credits");
		credits.ConnectClicked(OnClickCallback(this.OnCredits));
		credits.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnCredits));
		credits.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffCredits));
		@creditsLabel = controller.GetLabel("Credits");
		
		@selector = controller.GetWidget("Selector");
		
		setFontSize(48);
		setFontName("Times Sans Serif");
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	void OnPlay()
	{
		activateScene("SceneGame");
	}
	
	void OnMoveOnPlay()
	{
		selector.SetVisible(true);
		selector.SetY(play.GetY() + play.GetHeight()/2);
		selector.SetX(0.21f);
		playLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffPlay()
	{
		selector.SetVisible(false);
		playLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnMoveOnExit()
	{
		selector.SetVisible(true);
		selector.SetY(exit.GetY() + exit.GetHeight()/2);
		selector.SetX(0.22f);
		exitLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffExit()
	{
		selector.SetVisible(false);
		exitLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnExit()
	{
		quit();
	}
	
	void OnMoveOnSettings()
	{
		selector.SetVisible(true);
		selector.SetY(settings.GetY() + settings.GetHeight()/2);
		selector.SetX(0.05f);
		settingsLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffSettings()
	{
		selector.SetVisible(false);
		settingsLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnSettings()
	{
		activateScene("SceneSettings");
	}
	
	void OnMoveOnCredits()
	{
		selector.SetVisible(true);
		selector.SetY(credits.GetY() + credits.GetHeight()/2);
		selector.SetX(0.07f);
		creditsLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffCredits()
	{
		selector.SetVisible(false);
		creditsLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnCredits()
	{
		activateScene("SceneCredits");
	}
	
	Widget@ selector, play, exit, settings, credits;
	Label@ playLabel, exitLabel, settingsLabel, creditsLabel;
	GuiController@ controller;
}