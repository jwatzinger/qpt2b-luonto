class SceneNiklas : IScene
{
	SceneNiklas()
	{
		activateCamera("Free");
		setGameView("final scene hdr");
		
		@controller = loadController("Menu/HUD.axm");
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	GuiController@ controller;
}