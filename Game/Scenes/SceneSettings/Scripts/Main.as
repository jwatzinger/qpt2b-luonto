class SceneSettings : IScene
{
	SceneSettings()
	{
		@controller = loadController("Menu/Menu.axm");
		setGameView("Black");
		
		@back = controller.GetWidget("Back");
		back.ConnectClicked(OnClickCallback(this.OnBack));
		back.ConnectMoveOn(OnMoveOnCallback(this.OnMoveOnBack));
		back.ConnectMoveOff(OnMoveOffCallback(this.OnMoveOffBack));
		@backLabel = controller.GetLabel("Back");
		
		// ssao
		CheckBox@ ssao = controller.GetCheckBox("SSAOCB");
		ssao.SetChecked(getSetting("ssao").GetBool());
		ssao.ConnectChecked(OnCheckedCallback(this.OnChangeSSAO));
		
		// shadows
		CheckBox@ shadows = controller.GetCheckBox("ShadowCB");
		shadows.SetChecked(getSetting("shadows").GetBool());
		shadows.ConnectChecked(OnCheckedCallback(this.OnChangeShadows));
		
		Slider@ shadowQuality = controller.GetSlider("ShadowSlider");
		shadowQuality.SetValue(getSetting("shadowQuality").GetInt());
		shadowQuality.ConnectValueChanged(OnValueChangedCallback(this.OnChangeShadowQuality));
		
		// sight
		Slider@ sight = controller.GetSlider("SightSlider");
		sight.SetValue(getSetting("viewDistance").GetFloat());
		sight.ConnectValueChanged(OnValueChangedCallback(this.OnChangeSight));
		
		// trees
		Slider@ trees = controller.GetSlider("TreeSlider");
		trees.SetValue(getSetting("TreeDensity").GetFloat());
		trees.ConnectValueChanged(OnValueChangedCallback(this.OnChangeTrees));
		
		// grass
		Slider@ grass = controller.GetSlider("GrasSlider");
		grass.SetValue(getSetting("GrassDensity").GetFloat());
		grass.ConnectValueChanged(OnValueChangedCallback(this.OnChangeGrass));
		
		setFontSize(48);
		setFontName("Times Sans Serif");
	}
	
	void OnUninit()
	{
		controller.Unload();
	}
	
	void OnUpdate(double dt)
	{
	}
	
	void OnPlay()
	{
		activateScene("SceneGame");
	}
	
	void OnMoveOnBack()
	{
		backLabel.SetColor(Color(255, 255, 255));
	}
	
	void OnMoveOffBack()
	{
		backLabel.SetColor(Color(125, 125, 125));
	}
	
	void OnBack()
	{
		refreshSettings();
		activateScene("SceneMenu");
	}
	
	void OnChangeSSAO(bool ssao)
	{
		Setting@ setting = getSetting("ssao");
		setting.SetBool(ssao);
	}
	
	void OnChangeShadows(bool shadows)
	{
		Setting@ setting = getSetting("shadows");
		setting.SetBool(shadows);
	}
	
	void OnChangeShadowQuality(float shadowQuality)
	{
		Setting@ setting = getSetting("shadowQuality");
		setting.SetInt(int(shadowQuality));
	}
	
	void OnChangeSight(float value)
	{
		Setting@ setting = getSetting("viewDistance");
		setting.SetFloat(value);
		
		Setting@ setting2 = getSetting("VegViewDistance");
		setting2.SetFloat(value / 12.5f);
	}
	
	void OnChangeTrees(float value)
	{
		Setting@ setting = getSetting("TreeDensity");
		setting.SetFloat(value);
	}
	
	void OnChangeGrass(float value)
	{
		Setting@ setting = getSetting("GrassDensity");
		setting.SetFloat(value);
	}
	
	
	Widget@ back;
	Label@ backLabel;
	GuiController@ controller;
}