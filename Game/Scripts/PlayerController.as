enum InputAction
{
	PLAYER_JUMP
}

enum InputState
{
	PLAYER_MOVE_FORWARD,
	PLAYER_MOVE_BACKWARD,
	PLAYER_MOVE_LEFT,
	PLAYER_MOVE_RIGHT,
	SHIFT
};

enum InputRanges
{
	PLAYER_ZOOM,
	PLAYER_LOOK_X,
	PLAYER_LOOK_Y,
};

const float MAX_SPRINT_TIME = 5.0f;
const float EXHAUST_TIME = 2.5f;

// Game Process: Collected Schreine + Count
int SCHREIN_COUNT = 10;
int SCHREIN_COLLECTED = 0;

const float ZOOM_SPEED = 2.5f;
const float MIN_ZOOM = 4.0f;
const float MAX_ZOOM = 10.0f;
// the larger, the less zoom with each wheel turn
const float ZOOM_FACTOR = 75.0f;

const float LOOK_SPEED = 0.1f;
const float CAP_Y_ANGLE = 45.0f;

float MOVE_SPEED = 80.0f;

const float MAX_ANIM_SPEED = 1.0f;
const float MIN_SPEED_FOR_ANIM = 0.5f;

class TextBoxTrigger
{
	TextBoxTrigger()
	{
		hasText = false;
		closeBox = false;
		Text = "";
	}
	
	bool hasText, closeBox;
	string Text;
	
	void Collision(string text)
	{
		hasText = true;
		log(Text);
		Text = text;
	}
	
	bool HasText() const
	{
		return hasText;
	}
	
	void CloseBox()
	{
		closeBox = true;
	}
	
	bool CheckCloseBox()
	{
		if(closeBox)
		{
			closeBox = false;
			return true;
		}
		else
			return false;
	}
	
	string PopText()
	{
		string text = Text;
		Text = "";
		hasText = false;
		return text;
	}
}

TextBoxTrigger TEXT_BOX_TRIGGER;

class PlayerController : IGameScript
{	
	/*
	enum Animation
	{
		IDLE,
		WALK,
		RUN
	};*/

	bool inCollision; 
	bool isShiftPressed;
	int animation;
	
	PlayerController(const Entity& in ent)
	{
		entity = ent;
		inCollision = false;
		isShiftPressed = false;
		animation = 0;
		currentZoom = (MAX_ZOOM + MIN_ZOOM) / 2.0f;
		toZoom = 0.0f;
		angleX = 0.0f;
		angleY = 0.0f;
		toLookX = 0.0f;
		toLookY = 0.0f;
		moveStraight = 0.5f;
		moveSide = 0.0f;
		animSpeed = 0.0f;
		sprintPower = MAX_SPRINT_TIME;
		exhausted = 0.0f;
		jump = false;
		canJump = true;
		isTextShowing = false;
	}
	
	void OnInit()
	{
		Body@ body = GetBody(entity);
		body.body.SetDamping(0.8f);
		body.body.SetAngularFactors(0.0f, 0.0f, 0.0f);
		body.ConnectOnCollision(OnBodyCollisionCallback(this.OnCollision));
		
		Transform@ transform = GetTansform(entity);
		transform.offset.y = -0.9f;
	}
	
	void OnUpdate(double dt)
	{
		animation = 0;
		Cam@ camera = GetCamera(entity);
		if(!camera.active)
			return;
		
		Body@ body = GetBody(entity);
		
		UpdateCameraValues(dt);
		
		// lock camera to entity
		Position@ position = GetPosition(entity);
		const Vector3 vLookAt = position.GetVec() + Vector3(0.0f, pivot, 0.0f);
		camera.camera.SetLookAt(vLookAt);
		
		// calculate camera position offset
		Vector3 vLook = Vector3(1.0f, 0.0f, 0.0f);
		
		// horizontal rotation
		const Matrix mLookX = MatRotationY(degToRad(angleX));
		vLook = mLookX.TransformNormal(vLook);
		vLook.Normalize();
		
		// vertical rotation
		const Vector3 vUp = Vector3(0.0f, 1.0f, 0.0f);
		const Vector3 vRight = vUp.Cross(vLook).normal();
		const Matrix mLookY = MatRotationAxis(vRight, degToRad(angleY));
		vLook = mLookY.TransformNormal(vLook);
		vLook.Normalize();
		camera.camera.SetPosition(vLookAt +  vLook * currentZoom);	
		
		// TODO: optimize this, only set when camera really changed, includes caching of body position
		camera.dirty = true;
		
		// apply look to entity
		const Vector3 vLookXZ = Vector3(vLook.x, 0.0f, vLook.z).normal();
		// TODO: this is a hackfix to fix this one model, undo at times
		
		if(!canJump && !body.body.IsFalling())
			canJump = true;
			
		if(jump)
		{
			body.body.SetLinearVelocity(Vector3(0.0, 25.0f, 0.0f));
			jump = false;
			canJump = false;
		}

		//body.body.StopMovement();
		if(moveStraight != 0 || moveSide != 0)
		{
			const Vector3 vMoveRight = vLookXZ.Cross(vUp);
			Vector3 vMove = (vLookXZ * moveStraight + vMoveRight * moveSide).normal();
			if (exhausted > 0.0f)
			{
				exhausted -= dt;
				body.body.SetLinearVelocity(-vMove * MOVE_SPEED * dt);
				animation = 1;
			}
			else
			{
				if (isShiftPressed && (sprintPower>0.0f))
				{
					animation = 2;
					body.body.SetLinearVelocity(-vMove * 2 * MOVE_SPEED * dt);
					sprintPower -= dt;
					
					if(sprintPower <= 0.0f)
					{
						sprintPower = MAX_SPRINT_TIME;
						exhausted = EXHAUST_TIME;
					}
				}
				else 
				{
					if(sprintPower < MAX_SPRINT_TIME)
						sprintPower += dt;
						
					animation = 1;
					body.body.SetLinearVelocity(-vMove * MOVE_SPEED * dt);
				}
			}

			moveStraight = 0;
			moveSide = 0;
			
			animSpeed += dt*5;
		}
		else
			animSpeed -= dt*5;
			
		animSpeed = max(MIN_SPEED_FOR_ANIM, min(animSpeed, MAX_ANIM_SPEED));

		Vector3 vVelocity = body.body.GetVelocity();
		
		Direction@ dir = GetDirection(entity);
		// TODO: this is a hackfix to fix this one model, undo at times
		const Vector3 vDir = Vector3(-vVelocity.z, 0.0f, vVelocity.x);
		if(vDir.length() >= 0.5f)
		{			
			//log(vDir.length());
			dir.SetVec(vDir.normal());
		}
		
		Animation@ anim = GetAnimation(entity);
		const float speed = vVelocity.length();
		
		if(animation == 0 && speed >= dt * 2.0f)
			animation = 1;
		
		if(animation==0)
		{
			anim.SetAnimation("Idle");
			anim.speed = 1.0f;
		}
		else if(animation==1)
		{
			anim.SetAnimation("Walk");
			anim.speed  = max(speed / 10.0f, animSpeed);
		}
		else if(animation==2)
		{
			anim.SetAnimation("Run");
			anim.speed = 1.0f;
		}
	}
	
	bool UpdateCameraValues(double dt)
	{
		bool hasChanged = false;
		
		// update zoom
		if(toZoom != 0.0f)
		{
			const float zoomNow = toZoom * dt * ZOOM_SPEED;
			toZoom -= zoomNow;
			
			if((zoomNow > 0.0f && toZoom < 0.0f) ||
			    (zoomNow < 0.0f && toZoom > 0.0f))
				toZoom = 0.0f;
				
			currentZoom += zoomNow;
			
			// cap zoom at min/max distance
			if(currentZoom >= MAX_ZOOM)
			{
				currentZoom = MAX_ZOOM;
				toZoom = 0.0f;
			}
			else if(currentZoom <= MIN_ZOOM)
			{
				currentZoom = MIN_ZOOM;
				toZoom = 0.0f;	
			}
			
			hasChanged = true;
		}
		
		// update look on x-axis
		if(toLookX != 0.0f)
		{
			const float lookNow = toLookX * LOOK_SPEED;
			toLookX = 0.0f;
				
			angleX -= lookNow;
			
			if(angleX >= 360.0f)
				angleX -= 360.0f;
			else if(angleX <= 0.0f)
				angleX += 360.0f;
						
			hasChanged = true;
		}
		
		// update look on x-axis
		if(toLookY != 0.0f)
		{
			const float lookNow = toLookY * LOOK_SPEED;
			toLookY = 0.0f;
				
			angleY -= lookNow;
			
			if(angleY >= CAP_Y_ANGLE)
				angleY = CAP_Y_ANGLE;
			else if(angleY <= -CAP_Y_ANGLE)
				angleY = -CAP_Y_ANGLE;

			hasChanged = true;
		}
		
		return hasChanged;
	}
	
	void OnProcessInput(const MappedInput@ input)
	{
		if(isTextShowing)
		{
			if(input.HasAction(PLAYER_JUMP))
			{
				TEXT_BOX_TRIGGER.CloseBox();
				isTextShowing = false;
				
				if (SCHREIN_COUNT == SCHREIN_COLLECTED)
				{
					activateScene("SceneCredits");
				}
			}
		}
		
		Cam@ camera = GetCamera(entity);
		if(!camera.active)
			return;
			
		// movement
		if(input.HasState(PLAYER_MOVE_FORWARD))
			moveStraight = 1;
		if(input.HasState(PLAYER_MOVE_BACKWARD))
			moveStraight = -1;
			
		if(canJump)
		{
			if(input.HasState(PLAYER_MOVE_LEFT))
				moveSide = 1;
			if(input.HasState(PLAYER_MOVE_RIGHT))
				moveSide = -1;
		}
		if(canJump && input.HasAction(PLAYER_JUMP))
			jump = false;
		
		// zoom
		float zoom = 0.0f;
		if(input.HasRange(PLAYER_ZOOM, zoom))
			toZoom += zoom / ZOOM_FACTOR;
			
		// camera rotation
		if(canJump)
		{
			input.HasRange(PLAYER_LOOK_X, toLookX);
			input.HasRange(PLAYER_LOOK_Y, toLookY);
		}
		
		isShiftPressed = input.HasState(SHIFT);
	}
	
	void OnCollision(Entity& in otherEntity)
	{
		string name = otherEntity.GetName();
		
		if (!inCollision)
		{
			if (name.contains("Selector"))
			{
				inCollision = true;
				removeEntity(otherEntity);
				SCHREIN_COLLECTED++;
				MOVE_SPEED += 2;
				
				Entity soundEntity = createEntity();
				AttachAudio(soundEntity, "SchreinActivation", false, true);
				
				if(name == "SchreinSeaSelector")
					TEXT_BOX_TRIGGER.Collision("Der Erfolg ist jedem sicher, der ihn sich wirklich wuenscht. Unterschaetze niemals Deine Traeume!\nDu musst einen Pakt mit ihnen schliessen. Sie sind die Quelle einer unerschoepflichen Kraft,\ndie Dir erlaubt zu siegen. Hinter dem Hindernis oeffnet sich eine ganz neue Freiheit, ein viel weiterer Horizont.\n\n...\"Ich habe mir immer gewuenscht die Welt auf eigene Faust zu erkunden ...\n... frei zu sein.\"");
				else if(name == "GravestoneSelector")
					TEXT_BOX_TRIGGER.Collision("Fan Luang - Ruhe in Frieden.\n\nDieses Grab scheint nur halbherzig aufgeschuettet worden zu sein.\n...\n\"Was dieser Mann wohl verbrochen hat um so respektlos und so weit von zu Hause bestattet worden zu sein?\"");
				else if(name == "SchreinTreeSelector")
					TEXT_BOX_TRIGGER.Collision("Ein Schrein wie die vielen anderen in dieser Gegend. Dieser scheint erst kuerzlich erbaut worden zu sein.\n...\nDu bist nicht auf der Erde, um ungluecklich zu werden. Doch Glueck ist allein der innere Frieden.\nLerne ihn finden. Du kannst es. Ueberwinde dich selbst und du wirst die Welt ueberwinden.\n...\nMeine Mutter richtete diese Worte an mich als sie uns verliess.\n... oft zog sie sich hier an diesen Baum zurueck um zu meditieren.");
				else if(name == "FireplaceSelector")
					TEXT_BOX_TRIGGER.Collision("Eine kleine aber anscheinend oft und erst kuerzlich verwendete Feuerstelle.\n...\nDiese Feuerstelle kommt mir bekannt vor ...\n...\nIch erinnere mich an viele kalte Winternaechte die ich hier verbracht habe. Doch wieso?");
				else if(name == "BoatSelector")
					TEXT_BOX_TRIGGER.Collision("Das Boot sieht so aus als waere es schon laenger nicht mehr verwendet worden.\nAuch der See scheint frueher groesser gewesen zu sein.\n...\nGerne habe ich hier die Innenwaende des Bootes zusammen mit meiner Mutter\nmit kleinen Schnitzerein verziehrt.\nIch genoss jede Sekunde die ich mit Angeln und dem Lauschen der Wellen auf der Weite des Sees,\nvernab von Zuhause, verbringen konnte.");
				else if(name == "HouseSelector")
					TEXT_BOX_TRIGGER.Collision("Auf einem kleinen Schild neben der Eingangstuere steht in fast verblassten Buchstaben: Luang\n...\nIch erinnere mich wie sich der Schmerz meiner von Blasen übersaehten Haenden\nmit dem Geraeusch der monotonen Axthiebe und dem Geschrei von Drinnen zu einem dumpfen Schwall\nin meinem Kopf vermischten.\n");
				else if(name == "SchreinWegSelector1")
					TEXT_BOX_TRIGGER.Collision("Wir sind, was wir denken. Alles was wir sind, entsteht mit unseren Gedanken.\nMit unseren Gedanken machen wir die Welt.\"\n\nDiesen Weg bin ich gern gegangen...");
				else if(name == "SchreinWegSelector2")
					TEXT_BOX_TRIGGER.Collision("\"Hab Geduld mit jedem Tag deines Lebens.\"\n\n...\n\nDieser Weg fuehrt zum See voller Fische...");
				else if(name == "SchreinSelectorStart")
					TEXT_BOX_TRIGGER.Collision("Auf dem Schrein steht geschrieben:\n\n\"Es gibt nur eine Zeit, in der es wesentlich ist aufzuwachen. Diese Zeit ist jetzt.\"\n\nWo bin ich?\n...\nWas ist geschehn?");
				else if(name == "SchreinSelectorGeneration")
					TEXT_BOX_TRIGGER.Collision("\"In der Wut verliert der Mensch seine Intelligenz.\"\n\n...\nWenn ich mich richtig erinnere wurden diese Schreine bereits seit Generationen von meiner Familie errichtet.");
				
				isTextShowing = true;
				
				inCollision = false;
			}
		}
	}
	
	Entity entity;
	float pivot;
	bool isTextShowing;
	private int moveStraight, moveSide;
	private float currentZoom, toZoom;
	private float angleX, angleY, toLookX, toLookY;
	private float animSpeed;
	private float sprintPower, exhausted;
	private bool jump, canJump;
}